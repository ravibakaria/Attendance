-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 12, 2018 at 05:35 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newtest`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` int(10) UNSIGNED NOT NULL,
  `worker_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('IN','Out') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `todate` date NOT NULL,
  `in_time` datetime NOT NULL,
  `out_time` datetime NOT NULL,
  `deparment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `at` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `worker_id`, `name`, `status`, `todate`, `in_time`, `out_time`, `deparment`, `at`, `created_at`, `updated_at`) VALUES
(75, '1', 'ravi', 'IN', '2018-09-03', '2018-09-03 10:27:25', '0000-00-00 00:00:00', 'Labour', '0', '2018-09-04 04:57:25', '2018-09-07 10:04:22'),
(76, '2', 'raj', 'IN', '2018-09-03', '2018-09-03 10:27:28', '0000-00-00 00:00:00', 'Labour', '0', '2018-09-04 04:57:28', '2018-09-07 10:04:29'),
(79, '1', 'ravi', 'Out', '2018-09-04', '2018-09-04 13:47:24', '2018-09-04 13:47:51', 'Labour', '0', '2018-09-04 08:17:24', '2018-09-07 10:04:36'),
(80, '1', 'ravi', 'Out', '2018-09-06', '2018-09-06 08:00:56', '2018-09-06 12:56:15', 'Labour', '0', '2018-09-06 02:30:56', '2018-09-07 10:04:47'),
(81, '2', 'raj', 'Out', '2018-09-06', '2018-09-06 08:01:01', '2018-09-06 12:56:17', 'Labour', '0', '2018-09-06 02:31:01', '2018-09-07 10:04:53'),
(82, '3', 'amit', 'Out', '2018-09-06', '2018-09-06 08:01:04', '2018-09-06 12:56:20', 'Labour', '0', '2018-09-06 02:31:04', '2018-09-07 10:05:01'),
(83, '4', 'Vaishali', 'Out', '2018-09-06', '2018-09-06 08:01:08', '2018-09-06 12:56:23', 'Labour', '0', '2018-09-06 02:31:08', '2018-09-07 10:05:07'),
(84, '1', 'ravi', 'Out', '2018-09-07', '2018-09-07 07:49:22', '2018-09-07 13:31:16', 'Labour', '0', '2018-09-07 02:19:22', '2018-09-07 13:31:16'),
(85, '2', 'raj', 'Out', '2018-09-07', '2018-09-07 07:49:25', '2018-09-07 13:31:19', 'Labour', '0', '2018-09-07 02:19:25', '2018-09-07 13:31:19'),
(86, '3', 'amit', 'Out', '2018-09-07', '2018-09-07 07:49:28', '2018-09-07 13:31:22', 'Labour', '0', '2018-09-07 02:19:28', '2018-09-07 13:31:22'),
(87, '4', 'Vaishali', 'Out', '2018-09-07', '2018-09-07 07:49:31', '2018-09-07 13:31:24', 'Labour', '0', '2018-09-07 02:19:31', '2018-09-07 13:31:24'),
(88, '1', 'ravi', 'Out', '2018-08-01', '2018-08-01 00:00:00', '2018-08-01 00:00:00', '', '', NULL, '2018-09-07 10:06:06'),
(89, '1', 'ravi', 'Out', '2018-09-08', '2018-09-08 07:26:09', '2018-09-08 13:30:31', 'Labour', '0', '2018-09-08 01:56:09', '2018-09-08 13:30:31'),
(90, '2', 'raj', 'IN', '2018-09-08', '2018-09-08 07:26:14', '0000-00-00 00:00:00', 'Labour', '0', '2018-09-08 01:56:14', '2018-09-08 01:56:14'),
(91, '3', 'amit', 'IN', '2018-09-08', '2018-09-08 07:26:18', '0000-00-00 00:00:00', 'Labour', '0', '2018-09-08 01:56:18', '2018-09-08 01:56:18'),
(92, '4', 'Vaishali', 'IN', '2018-09-08', '2018-09-08 07:26:22', '0000-00-00 00:00:00', 'Labour', '0', '2018-09-08 01:56:22', '2018-09-08 01:56:22'),
(93, '1', 'ravi', 'IN', '2018-09-10', '2018-09-10 07:45:02', '0000-00-00 00:00:00', 'Labour', '0', '2018-09-10 02:15:02', '2018-09-10 02:15:02'),
(94, '2', 'raj', 'IN', '2018-09-10', '2018-09-10 07:45:05', '0000-00-00 00:00:00', 'Labour', '0', '2018-09-10 02:15:05', '2018-09-10 02:15:05'),
(95, '3', 'amit', 'IN', '2018-09-10', '2018-09-10 07:45:08', '0000-00-00 00:00:00', 'Labour', '0', '2018-09-10 02:15:08', '2018-09-10 02:15:08'),
(96, '4', 'Vaishali', 'IN', '2018-09-10', '2018-09-10 07:45:10', '0000-00-00 00:00:00', 'Labour', '0', '2018-09-10 02:15:10', '2018-09-10 02:15:10'),
(97, '5', 'rajesh', 'IN', '2018-09-10', '2018-09-10 08:33:43', '0000-00-00 00:00:00', 'Labour', '0', '2018-09-10 03:03:43', '2018-09-10 03:03:43'),
(98, '1', 'ravi', 'Out', '2018-09-11', '2018-09-11 07:43:43', '2018-09-11 12:37:45', 'Labour', '0', '2018-09-11 02:13:43', '2018-09-11 12:37:45'),
(99, '2', 'raj', 'Out', '2018-09-11', '2018-09-11 07:43:47', '2018-09-11 12:37:50', 'Labour', '0', '2018-09-11 02:13:47', '2018-09-11 12:37:50'),
(100, '3', 'amit', 'Out', '2018-09-11', '2018-09-11 07:43:50', '2018-09-11 12:37:54', 'Labour', '0', '2018-09-11 02:13:50', '2018-09-11 12:37:54'),
(101, '4', 'Vaishali', 'Out', '2018-09-11', '2018-09-11 07:43:53', '2018-09-11 12:37:57', 'Labour', '0', '2018-09-11 02:13:53', '2018-09-11 12:37:57'),
(102, '5', 'rajesh', 'Out', '2018-09-11', '2018-09-11 07:43:55', '2018-09-11 12:38:00', 'Labour', '0', '2018-09-11 02:13:55', '2018-09-11 12:38:00'),
(103, '1', 'ravi', 'Out', '2018-09-12', '2018-09-12 07:46:49', '2018-09-12 15:01:43', 'Labour', '0', '2018-09-12 02:16:49', '2018-09-12 15:01:43'),
(104, '2', 'raj', 'Out', '2018-09-12', '2018-09-12 07:46:53', '2018-09-12 15:01:46', 'Labour', '0', '2018-09-12 02:16:53', '2018-09-12 15:01:46'),
(105, '3', 'amit', 'Out', '2018-09-12', '2018-09-12 07:46:55', '2018-09-12 15:01:49', 'Labour', '0', '2018-09-12 02:16:55', '2018-09-12 15:01:49'),
(106, '4', 'Vaishali', 'Out', '2018-09-12', '2018-09-12 07:46:57', '2018-09-12 15:01:51', 'Labour', '0', '2018-09-12 02:16:57', '2018-09-12 15:01:51'),
(107, '5', 'rajesh', 'Out', '2018-09-12', '2018-09-12 07:47:00', '2018-09-12 15:01:54', 'Labour', '0', '2018-09-12 02:17:00', '2018-09-12 15:01:54');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_08_26_081242_create_workers_table', 2),
(4, '2018_08_26_191523_create_attendances_table', 3),
(5, '2018_08_26_225613_Attendance', 4),
(6, '2018_09_12_081027_create_events_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `isAdmin` tinyint(4) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `isAdmin`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ravi', 'ravi@gmail.com', '$2y$10$PFEooqgHKnT0.vnVuVPRpeSpZNTaWaPw0.7EKRMlp1OSYDGetPX42', 'gIhXtRFGQA7v2Bw8c0O4RhoMmyd9ze5VzYCNkWebtX00G1IgmCBELKV4Bhlc', '2018-08-25 09:08:41', '2018-08-25 09:08:41'),
(2, 2, 'Raj', 'raj@gmail.com', '$2y$10$wK5irT99Bdpgi4V6iFQ1N.T44qLRhnqyQ080x//uwWMBn84oSjtMa', 'cKlB5UNuyVPovK0ahdUEokbCyvUYr6bd1mTSHKu3ablqmUz7CWjIsAqWblC3', '2018-08-25 09:17:24', '2018-08-25 09:17:24');

-- --------------------------------------------------------

--
-- Table structure for table `workers`
--

CREATE TABLE `workers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `joindate` date NOT NULL,
  `deparment` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `workers`
--

INSERT INTO `workers` (`id`, `name`, `joindate`, `deparment`, `image`, `created_at`, `updated_at`) VALUES
(1, 'ravi', '2018-08-01', 'Labour', '', '2018-08-26 03:39:14', '2018-08-28 08:59:27'),
(2, 'raj', '2018-08-15', 'Labour', '', '2018-08-26 03:40:28', '2018-08-28 08:59:19'),
(3, 'amit', '2018-08-26', 'Labour', 'EWYB9882.JPG', '2018-08-26 12:54:23', '2018-08-28 08:59:14'),
(4, 'Vaishali', '2018-08-21', 'Labour', 'Penguins.jpg', '2018-08-28 04:59:29', '2018-08-28 04:59:29'),
(5, 'rajesh', '2018-09-10', 'Labour', 'cat-pet-animal-domestic-104827.jpeg', '2018-09-10 03:03:02', '2018-09-10 03:03:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `workers`
--
ALTER TABLE `workers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `workers`
--
ALTER TABLE `workers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
