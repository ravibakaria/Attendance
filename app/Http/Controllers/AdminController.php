<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class AdminController extends Controller
{
    public function index()
    {	
		if(auth()->user()->isAdmin == 1){
		
        $posts = \DB::table('users')
            ->select('users.*')
            ->orderby('created_at','desc')
            ->paginate(3);

		return view('userslist')->with('posts',$posts);
		}else{
			echo "You are not Authorise";
		}
    }
	public function show($id)
    {
        $posts = users::find($id);
		return view('employee')->with('posts',$posts);
    }
}
