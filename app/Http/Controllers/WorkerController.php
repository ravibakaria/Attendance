<?php

namespace App\Http\Controllers;

use App\Worker;
use Illuminate\Http\Request;

class WorkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $employees = worker::all();
		return view('worker.worker',compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if($user =auth()->user('id')){
            return view('worker.addworker');
        }
       else{
           echo "Login Frist";
          
       }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {	
	
		$this->validate($request, [
        'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);
		  $fileName = null;
    if (request()->hasFile('file')) {
        $file = request()->file('file');
        $fileName =$file->getClientOriginalName() ;
        $file->move('uploads/', $fileName);    
		echo $fileName;
			
		 /*if ($request->hasFile('input_img')) {
        $image = $request->file('input_img');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $name);
        $this->save();*/
			
        $worker = new Worker();
		$worker->name = $request->get('name');
		$worker->joindate = $request->get('joindate');
		$worker->deparment = $request->get('deparment');
		$worker->image = $fileName;
		$worker->save();
 
    return redirect('worker')->with('success','Employee has been added');
    }
	}
    /**
     * Display the specified resource.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		//echo $id;
        //$posts = Post::find($id);
		//return view('posts.show')->with('posts',$posts);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$employee = Worker::find($id);
		
    return view('worker.workeredit',compact('employee','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $worker= worker::find($id);
		$worker->name=$request->get('name');
		$worker->joindate = $request->get('joindate');
		$worker->deparment = $request->get('deparment');
		$worker->save();
		return redirect('worker');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Worker::find($id);
		$employee->delete();
		return redirect('worker')->with('success','Employee Has Been Deleted');
    }
	
	public function intime(Request $request)
    {	
		$id= $request->get('id');
		//echo $id;
		// $user = \DB::select("select * from workers where id = '$id'");
		//print_r($user);
		//return view('worker.ravi',compact('user','id'));
		//return view('worker.ravi')->with('user',$user);
        //$posts = Post::find($id);
		//return view('posts.show')->with('posts',$posts
		$employee = Worker::find($id);
		
			return view('worker.intime',compact('employee','id'));
    }
	public function Outtime(Request $request)
    {	
		$id= $request->get('id');
		//echo $id;
		// $user = \DB::select("select * from workers where id = '$id'");
		//print_r($user);
		//return view('worker.ravi',compact('user','id'));
		//return view('worker.ravi')->with('user',$user);
        //$posts = Post::find($id);
		//return view('posts.show')->with('posts',$posts
		$employee = Worker::find($id);
		echo $employee->in_time;
			//return view('worker.outtime',compact('employee','id'));
    }
}
