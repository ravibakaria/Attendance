<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Maatwebsite\Excel\Facades\Excel;
use App\Worker;
use App\Attendance;
use Calendar; 
use \DB;
use Excel;
//use Maatwebsite\Excel\Excel;
class DownloadController extends Controller
{
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }
    
    public function export(Request $request){

        if(auth()->user()->isAdmin == 2){
        $id= $request->get('id');
        $date1=$request->get('fromdate');
        $date2=$request->get('todate');
     // $result1 = Attendance::all();
   $result1=\DB::select("SELECT worker_id,
   name As Name,todate As Date,
   time_format(in_time, '%l: %i %p') As In_time,
   time_format(out_time, '%l: %i %p') As Out_time ,
   TIMEDIFF(time(out_time),time(in_time)) AS Total_Time,
   date_format(in_time, '%W') As Day
  
   FROM attendances  
   WHERE worker_id = '$id' AND status != 'IN' AND todate BETWEEN '$date1' AND '$date2'"); 
    //  $result = DB::table('attendances')->where('worker_id', $id)
    //  ->whereBetween('todate', [$date1, $date2])->get(); 
    // var_dump($result);
    // echo "<hr>";
    // var_dump($result1);

    $result1 = json_decode(json_encode($result1), true);
    // $result= [  
    //     ['id'=>'2', 'name'=>'ravi','todate'=>'2018-09-08'],
    //     ['id'=>'3', 'name'=>'raj','todate'=>'2018-09-08']
    // ];
    $result=$result1;
        Excel::create('result', function($excel) use($result) {
            $excel->sheet('ExportFile', function($sheet) use($result) {
                $sheet->fromArray($result);
            });
        })->export('xlsx');
    }
    else{
        echo "not allow";
    }    
    }
    public function calendar(){  
        
       
        $events = [];
        $data =\DB::select("SELECT worker_id, name,todate,time_format(in_time, '%l: %i %p') As In_time,time_format(out_time, '%l: %i %p') As Out_time,out_time FROM attendances WHERE worker_fid = '2'"); 
        
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->In_time . " IN",
                    true,
                   
                    new \DateTime($value->todate),
                    new \DateTime($value->todate),
                    null,
                    // Add color and link on event
	                [
	                    'color' => '#f05050',
	                    
	                ]
                );
            }
            
            
        $calendar = Calendar::addEvents($events);
        
        return view('calendar', compact('calendar')); 
    }
}
