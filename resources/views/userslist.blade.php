@extends('layouts.app')
@section('content')

<h1>Users </h1>
@if(count($posts)>0)
	@foreach($posts as $post)
		<div class="well">
			<h3><a href=""> {{$post->name}}</a></h3>
			<small>Register on {{$post->created_at}}</small>
			<a href=""><button class="btn btn-link">make admin</button>
		</div>
	@endforeach
	{{$posts->links()}}
@else
	<p>No Posts Found</p>
@endif
@endsection