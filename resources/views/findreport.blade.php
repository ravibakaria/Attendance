@extends('layouts.app')
@section('content')

<div class="container">
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>No Of Days</th>
				
			</tr>
		</thead>
		<tbody>
			@foreach($result as $employee)
			<tr>
				<td>{{$employee->worker_id}}</td>
				<td>{{$employee->name}}</td>
				
            </tr>
            
			@endforeach
		</tbody>
        </table>
        
	</div>
@endsection