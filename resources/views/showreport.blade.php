@extends('layouts.app')
@section('content')


<div align="center">
		<h3>Find By Date Attendances</h3>
		@foreach($result as $employee)
		   <form class="form-horizontal" action="{{url('findattendances')}}" method="post" name="upload_excel"   
					 enctype="multipart/form-data">
		   	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		   	<input type="hidden" name="id" value="{{$employee->worker_id}}">
		   
			From 
			<input type="date" name="fromdate" id="">
			TO
			<input type="date" name="todate" id="">
			<input type="submit" name="Export" class="btn btn-success" value="export to excel"/>
		   </form>
	</div>
	<h3>Current Month Attendances</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>No Of Days</th>
				
			</tr>
		</thead>
		<tbody>
			
			<tr>
				<td>{{$employee->worker_id}}</td>
				<td>{{$employee->name}}</td>
				<td>{{$employee->workday}}</td>
            </tr>
            
			@endforeach
		</tbody>
        </table>
        
              
	</div>
@endsection