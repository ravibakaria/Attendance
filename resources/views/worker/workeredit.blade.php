@extends('layouts.app')
@section('content')
<div class="container">
  <h2>Update Employee</h2><br  />
	<form method="post" action="{{action('WorkerController@update', $employee->id)}}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input name="_method" type="hidden" value="PATCH">
	<div class="row">
	  <div class="col-md-12"></div>
	  <div class="form-group col-md-4">
		<label for="name">Name:</label>
		
		<input type="text" class="form-control" name="name" value="{{$employee->name}}">
	  </div>
	  <div class="col-md-12"></div>
	  <div class="form-group col-md-4">
		<label for="name">Join Date:</label>
		<input type="date" class="form-control" name="joindate" value="{{$employee->joindate}}">
	  </div>
	  <div class="col-md-12"></div>
	  <div class="form-group col-md-4">
		<label for="name">Deparment:</label>
		<input type="text" class="form-control" name="deparment" value="{{$employee->deparment}}">
	  </div>
	</div>
	
	<div class="row">
	  <div class="col-md-12"></div>
	  <div class="form-group col-md-12" style="margin-top:10px">
		<button type="submit" class="btn btn-success">Update</button>
	  </div>
	</div>
  </form>
</div>


@endsection