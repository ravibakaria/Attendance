 @extends('layouts.app')
@section('content')

<div class="container">
	<br />
	@if (\Session::has('success'))
		<div class="alert alert-success">
			<p>{{ \Session::get('success') }}</p>
		</div><br />
	@endif
	<button><a href="{{url('worker/create')}}">Add Worker</a></button>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Join Date</th>
				<th>Deparmeni</th>
			</tr>
		</thead>
		<tbody>
			@foreach($employees as $employee)
			<tr>
				<td>{{$employee['id']}}</td>
				<td>{{$employee['name']}}</td>
				<td>{{$employee['joindate']}}</td>
				<td>{{$employee['deparment']}}</td>

				<td align="right"><a href="{{action('WorkerController@edit', $employee['id'])}}" class="btn btn-warning">Edit</a></td>
				<td align="left">
					<form action="{{action('WorkerController@destroy', $employee['id'])}}" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input name="_method" type="hidden" value="DELETE">
					<button class="btn btn-danger" type="submit">Delete</button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
		</table>
	</div>
@endsection