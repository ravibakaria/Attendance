@extends('layouts.app')
@section('content')
<div class="container">
  <h2>Worker Details</h2><br/>
  <div style ="float:left;margin-top:30px; ">
		
		<img src="uploads/{{$employee->image}}" height="300" width = "300">
		
	</div>
	<div style ="float:left;  margin-left:40px; width:50%">
	<form action="{{url('attendance')}}" method="POST">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	
	
		<div class="row">
			<div class="col-md-12"></div>
		  <div class="form-group col-md-4">
			<label for="name">ID:</label>
			<input type="text" class="form-control" name="id" value="{{$employee->id}}">
		  </div>
		  <div class="col-md-12"></div>
		  <div class="form-group col-md-4">
			<label for="name">Name:</label>
			<input type="text" class="form-control" name="name" value="{{$employee->name}}">
		  </div>
		  <div class="col-md-12"></div>
		  <div class="form-group col-md-4">
			<label for="name">Join Date:</label>
			<input type="date" class="form-control" name="joindate" value="{{$employee->joindate}}">
		  </div>
		  <div class="col-md-12"></div>
		  <div class="form-group col-md-4">
			<label for="name">Deparment:</label>
			<input type="text" class="form-control" name="deparment" value="{{$employee->deparment}}">
		  </div>
		</div>
		
		<div class="row">
			<div class="col-md-12"></div>
			<div class="form-group col-md-12" style="margin-top:10px">
			<button type="submit" class="btn btn-success">OK</button>
			</div>
		</div>
	
  </form>
  </div>
</div>


@endsection