@extends('layouts.app')
@section('content')
<div class="container">
  <h2>Worker List</h2><br/>
  <form method="post" action="{{url('worker')}}" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="row">
	  <div class="col-md-12"></div>
	  <div class="form-group col-md-4">
		<label for="Name">Name:</label>
		<input type="text" class="form-control" name="name">
	  </div>
	  <div class="col-md-12"></div>
	  <div class="form-group col-md-4">
		<label for="Name">Join Date:</label>
		<input type="date" class="form-control" name="joindate">
	  </div>
	  <div class="col-md-12"></div>
	  <div class="form-group col-md-4">
		<label for="Name">Deparment:</label>
		<input type="text" class="form-control" name="deparment">
	  </div>
	  <div class="col-md-12"></div>
	  <div class="form-group col-md-4">
		<label for="Name">Select image to upload:</label>
		<input type="file" name="file" id="file" class="form-control">
	  </div>
	</div>
	<div class="row">
	  <div class="col-md-12"></div>
	  <div class="form-group col-md-4" style="margin-top:10px">
		<button type="submit" class="btn btn-success">Submit</button>
	  </div>
	</div>
  </form>
</div>


@endsection