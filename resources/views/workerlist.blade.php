@extends('layouts.app')
@section('content')


<div class="container">	
		
@if(auth()->user()->isAdmin == 2)
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Join Date</th>
					<th>Deparment</th>
				</tr>
			</thead>
			<tbody>
				@foreach($employees as $employee)
				<tr>
					<td>{{$employee['id']}}</td>
					<td>{{$employee['name']}}</td>
					<td>{{$employee['joindate']}}</td>
					<td>{{$employee['deparment']}}</td>

					
					<td ><a href="report/{{$employee->id}}" class="btn btn-warning">View Attendance</a></td>
					
				</tr>
				@endforeach
			</tbody>
		</table>
		@else
		<h1>ravi</h1>
	@endif
</div>
@endsection