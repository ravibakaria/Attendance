<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function (){
	   return view('welcome');
   });
   Auth::routes();

   Route::get('/home', 'HomeController@index')->name('home');
   

   
// Route::get('admin/routes', 'HomeController@admin')->middleware('admin');
// Route::get('list', 'AdminController@index');
// //Route::post('intime', 'WorkerController@intime');

// Route::post('outtime', 'AttendanceController@outtime');
// Route::resource('worker', 'WorkerController');
// Route::resource('attendance', 'AttendanceController');
// Route::resource('report', 'ReportController');
// Route::post('findattendances', 'DownloadController@export');
// Route::get('calendar', 'DownloadController@calendar');
// //Route::get('event', 'DownloadController@event');
// Route::get('event', function (){
//    return view('holiday');
// });
// kyes: AIzaSyDeQJF0uHshKkM_cMtTa__Fi7WOxBIejiM
// kyes: d3e1de22-0b99-403f-a00c-e10047ee142a
// id : en.indian#holiday@group.v.calendar.google.com